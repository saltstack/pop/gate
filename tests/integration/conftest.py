import pathlib
import subprocess
import sys

import aiohttp
import pytest


@pytest.fixture(scope="session", autouse=True)
def gate():
    runpy = pathlib.Path(__file__).parent.parent.parent / "run.py"
    assert runpy.exists()
    gate_bin = subprocess.Popen(
        [
            sys.executable,
            str(runpy),
            "--port=5826",
            "--refs",
            "init.test*",
            "--prefix=gate",
            "--log-level=debug",
        ],
        stderr=subprocess.PIPE,
    )
    try:
        for line in gate_bin.stderr:
            if b"Uvicorn running on" in line:
                break
        else:
            raise ChildProcessError("Could not start server")

        yield
    finally:
        gate_bin.terminate()
        for line in gate_bin.stderr:
            if b"Finished server process" in line:
                break
        else:
            raise ChildProcessError("Could not terminate server properly")


@pytest.mark.asyncio
@pytest.fixture(scope="function")
async def requests():
    async with aiohttp.ClientSession() as client:
        yield client
